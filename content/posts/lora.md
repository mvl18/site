---
title: "Lora - Uma tecnologia que o software livre pode contribuir"
date: 2023-04-27T16:14:13-03:00
draft: false
tags: ["LoRa", "LoRaWAN", "LoRa Mesh", "Free software"]
categories: ["Hardware", "LoRa", "Bibliotecas"]
authors: ["Julio Nunes Avelar", "Tiago Silveira Zaparoli"]
---

# LoRa - Uma tecnologia que o software livre pode contribuir

## O que é LoRa?

LoRa é uma tecnologia de comunicação via rádio de baixa frequência que permite comunicação a longas distâncias com baixo consumo de energia, seu alcance gira em torno de 3 a 4 km em áreas urbanas e chega até aos 12 km em áreas rurais com ampla visada. Sua utilização mais comum é em dispositivos IoT (Internet of things / Internet das coisas) em aplicações desde sistemas de monitoramento como sensores a sistema de acionamento, e utilizada sobretudo em dispositivos que têm baixa capacidade energética como dispositivos mantidos em baterias, dispositivos em locais de difícil acesso, entre outros. Apenas para atributo de curiosidade a sigla LoRa significa Long Range (em tradução livre longo alcance).


## Por que usar LoRa?

LoRa, assim como todas as tecnologias de comunicação, tem suas vantagens e desvantagens, com a maior desvantagem sendo sua baixa largura de banda. Você pode se perguntar o porquê da utilização de LoRa ao invés de tecnologias como Wi-Fi, GSM e Bluetooth, por exemplo. Essas tecnologias citadas têm vantagens como a facilidade de utilização, devido a ampla literatura disponível, capacidade de largura de banda considerável, e, em casos como o GSM, amplo alcance. A maior vantagem do LoRa sobre essas tecnologias citadas que justifica a sua utilização é o baixo consumo de energia e o longo alcance sem depender de infraestruturas de terceiros (como é o caso do GSM). Dispositivos com comunicação via Wi-Fi e Bluetooth têm seu alcance limitado a casa dos 200 metros na melhor das hipóteses, e um consumo de energia muito elevado levando em conta as características de dispositivos IoT, enquanto a utilização de GSM fornece um alcance muito amplo devido a grande cobertura de rede móvel disponível atualmente, apesar de seu consumo ser relativamente alto e dependente de infraestrutura das operadoras da rede. Sendo essas e outras lacunas que favorecem a utilização de LoRa sobre essas e outras tecnologias principalmente em casos onde o longo alcance e baixo consumo precisam andar lado a lado.


##  Protocolos para LoRa.

Assim como outras tecnologias de comunicação, LoRa também possui alguns protocolos que rodam sobre ela, como é o caso do LoRaWAN e LoRa Mesh.


### LoRaWAN.

LoRaWAN é o nome do principal protocolo utilizado com LoRa, ele define desde a arquitetura do sistema aos parâmetros de comunicação utilizando LoRa. Atualmente o protocolo LoRaWAN é utilizado de forma majoritária por quase todas as aplicações LoRa existentes. Ele implementa detalhes de funcionamento, segurança, qualidade do serviço, ajustes de potência e os tipos de aplicações utilizadas tanto do lado do dispositivo IoT como do lado do servidor.


#### Arquitetura.

O protocolo LoRaWAN utiliza uma topologia em camadas, com uma topologia em estrela sendo utilizada para comunicação entre os dispositivos LoRa, com todos os dispositivos baseados em LoRa se comunicando com um gateway e esse gateway se comunicando com o restante da rede, com esse gateway funcionando como uma espécie de ponte enviando informações da rede para os dispositivos e dos dispositivos para rede.


![Arquiterua LoRaWAN](/imgs/posts/lora/arquitetura-lorawan.png)
Topologia LoraWAN - fonte UFRJ


#### Dispositivos Endpoints

São os dispositivos básicos da rede como sensores e atuadores, com estes sendo a camada mais externa da rede e utilizam apenas LoRa para comunicação.


#### Gateways

São os dispositivos responsáveis, como já dito anteriormente, por realizar a ponte entre os endpoints e o restante da rede (roteadores, servidores, etc). No geral esses dispositivos possuem capacidade de comunicação por 2 ou mais meios, sendo comumente LoRa e algum outro tipo de tecnologia de comunicação, como LoRa e Wi-Fi ou LoRa e Ethernet, por exemplo. Um gateway pode receber dados de milhares de dispositivos e encaminhá-los para um servidor de rede. A cobertura de um gateway depende apenas da potência dos seus módulos LoRa e da topologia do local onde ele está instalado com o raio de cobertura variando entre 2 e 15 km de acordo com estes parâmetros.


#### Classes de dispositivos

Para atender diversas demandas distintas, no protocolo LoRaWAN foram definidas três classes de dispositivos:



1. Classe A - Sensores: Comunicação bi-direcional, recepção após transmissão. (Os módulos só podem receber dados em janelas de tempo pré determinadas e imediatamente após realizarem uma transmissão).
2. Classe B - Atuadores: Comunicação bi-direcional, com janelas de recepção agendadas.
3. Classe C - Bidirecional, com recepção de dados quase a todo momento: Nesta classe o módulo sempre está apto a receber dados do gateway.


#### Segurança

O protocolo LoRaWAN fornece dois níveis de segurança, com a parte do pacote que contém o “payload” sendo encriptado com criptografia AES 128 e com o pacote como um todo sendo assinado utilizando criptografia AES 128 para garantir a integridade do pacote enviado, garantindo assim que o pacote não foi alterado por erros ou propositalmente.


![Segurança do LoRaWAN](/imgs/posts/lora/Lora-seguranca.webp)
Segurança do protocolo LoRaWAN - Fonte: Embarcados


#### Vantagens e desvantagens

LoRaWAN tem diversas vantagens, como a alta eficiência energética, fácil implementação, literatura disponível com bastante abundância e padrões bem definidos. Por outro lado é um protocolo altamente centralizado, onde para se ter redundância você fica obrigado a ter gateways com raios de cobertura sobrepostos, além disso, para locais com relevo com diversos obstáculos, os gateways tem seu alcance limitado, fazendo com que tenha se gasto com repetidores ou outros gateways.


### LoRa Mesh

Começando a explicação pelo nome Mesh, mesh significa malha ou seja uma rede mesh é uma rede em malha onde se tem diversos nós e esses nós conseguem se comunicar entre si. Em uma rede mesh quanto mais nós a rede tem maior o seu alcance, mas em compensação o consumo de energia dos dispositivos da rede aumenta e a capacidade diminui, pois os nós começam a ter que processar e transmitir informações de outros nós. 


#### Arquitetura.

O protocolo LoRa Mesh utiliza uma topologia em malha, em que todos os dispositivos conseguem fazer uma mensagem chegar a outro sem necessariamente passar por um gateway. Para realizar a comunicação com o servidor a rede pode utilizar uma espécie de gateway, mas mais de um dispositivo pode assumir essa condição de gateway e fazer uma ponte com a internet.


![Topologia em Malha](/imgs/posts/lora/topologia-malha.jpeg)
Topologia em malha


#### Segurança

O protocolo LoRa Mesh não fornece nenhum padrão específico para segurança, com esse padrão ficando a cargo da implementação da rede. Para segurança na rede LoRa Mesh recursos do LoRaWAN podem ser reaproveitados.


#### Vantagens e desvantagens

A maior vantagem da rede LoRa Mesh é a alta disponibilidade e redundância da rede, além de sua cobertura muito maior, já que quanto mais dispositivos se tem na rede, maior sua capacidade de alcance. Por outro lado sua implementação é muito mais complexa e possui muito menos literatura disponível do que LoRaWAN, além de sua menor eficiência energética, que para dispositivos baseados em bateria é algo com grande impacto.


## Faixas de operação.

LoRa opera em faixas de frequências de 433MHz a 928 MHz, com cada país tendo uma regulamentação específica. No Brasil as faixas regulamentadas pela Anatel são de 902 MHz a 907.5 MHz e de 915 MHz a 928 MHz.


![Faixa de operação](/imgs/posts/lora/faixa-operacao.webp)
Faixas de comunicação LoRa - Fonte: Eletrogate


## Redes Colaborativas.

Atualmente temos diversos projetos que tem como objetivo permitir o desenvolvimento de redes LoRa WAN com custos baixo, uma das formas de fazer isso é através de redes colaborativas onde cada indivíduo ou um grupo de indivíduos mantém um gateway por conta própria e permite a utilização deste gateway de forma gratuita por outras pessoas, tendo se uma rede com uma ampla cobertura em que qualquer pessoa pode usar e o custo de aquisição e manutenção se torna totalmente diluído, além de não depender de nenhuma entidade central para manutenção destes equipamentos. Uma grande iniciativa na área é a The Things Network (TTN). A The Things Network e uma rede IoT colaborativa de escala global que fornece uma estrutura cloud de baixo custo para conexão de gateways e aplicações baseadas em LoRa, assim toda vez que alguém compra ou monta um gateway e conecta a TTN, qualquer usuário da rede pode começar a usar aquele gateway para acessar a Cloud da TTN, desta forma se cria uma rede global cada vez maior de forma totalmente colaborativa e diluída.


## Desenvolvimento de Bibliotecas LoRa.

Atualmente existem diversas bibliotecas que permitem a utilização de LoRa em sua forma “pura” ou através do LoRaWAN, só que essas bibliotecas possuem suas limitações de implementações para certas arquiteturas, com muitos microcontroladores não possuindo um porte nativo para ele, ou quando existe a biblioteca está abandonada. A boa notícia é que por existir diversas bibliotecas prontas, existe algo já feito para se basear, necessitando-se apenas do porte para as tecnologias nativas de certos microcontroladores.


## Desenvolvimento do Protocolo LoRa Mesh.

LoRa Mesh é uma tecnologia que por mais que seja muito promissora, ainda permanece pouco implementada e documentada, com grande parte do que está atualmente feito tendo de forma proprietária, dificultando assim o seu desenvolvimento e utilização em larga escala por diversas pessoas. Desta forma há muito a se fazer, como o desenvolvimento de padrões, documentação, além da implementação prática mesmo de software para viabilizar o padrão LoRa Mesh.


## Como o software livre pode ajudar a difundir LoRa.

Como dito nos últimos tópicos, há muita coisa a se desenvolver para LoRa ainda, desde bibliotecas a protocolos completos, e, desta forma uma das possíveis soluções para resolver esse problema é a utilização de software livre, criando bibliotecas e protocolos completamente livres, permitindo assim a contribuição por parte de toda comunidade para o desenvolvimento e manutenção dessas tecnologias. Antes de finalizar este artigo gostaria de trazer algumas propostas de contribuições por parte da comunidade: atualmente existe uma falta de bibliotecas nativas para utilização de LoRa com as famílias de microcontroladores ESP32 (ESP32, ESP32S2, ESP32C3, ESP32S2, etc) usando o framework nativo o ESP-IDF; existe falta de bibliotecas nativas também para plataforma STM, entre outras; além disso existem diversos portes para utilização de outras linguagens e frameworks de desenvolvimento que não possuem bibliotecas para utilização de LoRa, como o zephyr RTOS, e a linguagem RUST em diversos microcontroladores; por último queria sugerir também a contribuição para a construção de um protocolo LoRa Mesh totalmente open source.

Algumas das sugestões citadas acima já possuem algumas iniciativas na área, algumas até de minha autoria. Como os seguintes projetos (atualizados no momento da escrita deste artigo, podendo, eventualmente sofrer alterações em seus detalhes) :

Biblioteca LoRa para o ESP-IDF: [https://github.com/JN513/esp32-lora-library](https://github.com/JN513/esp32-lora-library)

Biblioteca para utilização de Módulo LoRa via UART para o ESP-IDF: [https://github.com/JN513/esp32_uart_sx1276](https://github.com/JN513/esp32_uart_sx1276)

Gateway LoRaWAN baseado no ESP32: [https://github.com/JN513/lora_gateway](https://github.com/JN513/lora_gateway)

Implementação de LoRa Mesh: [https://github.com/JN513/Lora_Mesh_Network](https://github.com/JN513/Lora_Mesh_Network)

Gateway baseado no Raspberry PI: [https://github.com/tftelkamp/single_chan_pkt_fwd](https://github.com/tftelkamp/single_chan_pkt_fwd) 

Gateway baseado no Raspberry PI: [https://github.com/ch2i/LoraGW-Setup](https://github.com/ch2i/LoraGW-Setup)

Gateway LoRaWAN baseado no ESP32: [https://github.com/sparkfun/ESP32_LoRa_1Ch_Gateway](https://github.com/sparkfun/ESP32_LoRa_1Ch_Gateway)


## Fontes:

EMBARCADOS.  Conheça a tecnologia LoRa® e o protocolo LoRaWAN. Disponivel em: https://embarcados.com.br/conheca-tecnologia-lora-e-o-protocolo-lorawan/ Acesso em: 04/04/2023

Bertamoni, N. R. . Avaliação do esquema de modulação LoRa implementado em GNURadio e sintetizado em SDR. Disponível em: https://www.lume.ufrgs.br/bitstream/handle/10183/235783/001136770.pdf?sequence=1 Acesso em: 07/04/2023

UFRJ. Arquitetura da Rede. Disponivel em: https://www.gta.ufrj.br/ensino/eel878/redes1-2019-1/vf/lora/arquitetura.html Acesso em: 07/04/2023

ELETROGATE. Monitoramento de Temperatura com Heltec ESP32 LoRa. Disponível em: https://blog.eletrogate.com/monitoramento-remoto-de-temperatura-utilizando-a-heltec-esp32-lora/ Acesso em: 10/04/2023

Redes Informáticas. Topologias de rede. Disponivel em: https://redesinformticas-juca.blogspot.com/2009/11/topologias-de-rede.html Acesso em: 10/04/2023

EMBARCADOS. The Things Network: uma rede para IoT colaborativa. Disponivel em: https://embarcados.com.br/the-things-network-rede-iot/ Acesso em: 10/04/2023

EMBARCADOS. Gateways LoRa: soluções open-source hardware. Disponivel em: https://embarcados.com.br/gateways-lora-open-source-hardware/ Acesso em: 10/04/2023