---
title: "Bzóide: A Origem"
date: 2023-04-27T15:27:13-03:00
draft: false
tags: ["site"]
categories: ["Historia"]
authors: ["Julio Nunes Avelar"]
---

# 🤖📚 Bzóide: A Origem 🧐

Se você está lendo este post, é provável que esteja curioso(a) sobre o significado por trás do nome deste blog: Bzóide. 🤔 Afinal, é um nome um tanto quanto "diferente", não é mesmo?

Mas não se preocupe, pois estamos aqui para contar a você toda a história por trás desse nome enigmático. 📖

## 🕰️ Contexto Histórico

Primeiramente, é importante saber que o Bzóide tem suas raízes na Universidade Estadual de Campinas, mais conhecida como [UNICAMP](https://ic.unicamp.br/graduacao/engenharia-da-computacao/). 🎓 Nessa instituição, no curso de Engenharia da Computação, existem duas áreas de concentração: Sistemas de Computação (AA) e Sistemas e Processos Industriais (AB).

Os estudantes da área AA são carinhosamente chamados de "Azóides", enquanto os da área AB são conhecidos como "Bzóides". 😎

No entanto, há uma triste realidade por trás dessa história: a maioria dos alunos opta por Sistemas de Computação (AA) em vez de Sistemas e Processos Industriais (AB). 😞 Isso resulta em turmas com mais de 60 Azoides e apenas cerca de meia dúzia de Bzóides. 

Por esse motivo, existe uma lenda urbana na UNICAMP que Bzóides são apenas um mito e não existem na realidade. 😱 Mas a verdade é que esses estudantes são raros e, definitivamente, as pessoas mais legais e gente fina da universidade. Então, se você conhecer um bzoide, cuide bem dele, pois ele é realmente uma raridade. 😉

## 🤖🧐 O Significado do Nome

Agora, você deve estar se perguntando: "mas afinal, o que Bzoide significa?". Bem, a resposta é simples: é uma abreviação do nome dado aos estudantes da área AB do curso de Engenharia da Computação da UNICAMP. 😁

E é exatamente por isso que escolhemos esse nome para o nosso blog. Queremos provar para o mundo que Bzóides existem e são muito mais do que um simples mito. 😉 

## 📚 Conclusão

Em resumo, o nome Bzoide é uma homenagem aos estudantes da área de Sistemas e Processos Industriais do curso de Engenharia da Computação da UNICAMP. 🎓 E, como tal, é uma representação da raridade e da singularidade desses alunos dentro da universidade.

Esperamos que agora você entenda melhor o significado por trás do nome deste blog e fique conosco para acompanhar nossas publicações. 🤖📚