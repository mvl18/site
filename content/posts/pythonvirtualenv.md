---
title: "Ambientes virtuais em Python"
date: 2023-04-28T10:24:28-03:00
draft: false
categories: ["utilitarios"]
tags: ["Python", "venv"]
authors: ["Julio Nunes Avelar"]
---

# 🐍 Ambientes virtuais em Python 🐍

Imagina só: você tem dois projetos em Python que usam a biblioteca numpy, mas um precisa da versão 1.5 e o outro da versão 1.3. O que fazer? Instalar só uma versão no seu computador e sacrificar um dos projetos? Não precisa! Com os ambientes virtuais em Python, você pode ter instâncias isoladas do Python para cada projeto! 🤯

## Mas, peraí, o que são esses ambientes virtuais? 🤔

Um ambiente virtual Python é um diretório que contém uma instalação Python isolada, com as bibliotecas e dependências necessárias para um projeto específico. Eles permitem que você isole diferentes projetos uns dos outros, garantindo que cada um tenha seu próprio conjunto de bibliotecas e configurações, sem interferir uns com os outros. 🤓

## E por que você deveria usar um ambiente virtual Python? Aqui estão algumas razões: 🤔

- 👉 Isolamento de pacotes: você pode instalar pacotes específicos de um projeto sem interferir com outros projetos ou com a instalação global do Python.
- 👉 Consistência do ambiente: você pode garantir que todas as pessoas envolvidas em um projeto estejam usando exatamente o mesmo ambiente, ajudando a garantir a consistência nos resultados.
- 👉 Facilidade de configuração: ao criar um ambiente virtual Python, ele vem com uma cópia do interpretador Python e das bibliotecas padrão, o que significa que você não precisa se preocupar em instalar ou configurar o Python em cada máquina.
- 👉 Portabilidade: um ambiente virtual Python pode ser facilmente movido de uma máquina para outra, o que pode ser útil se você precisar trabalhar em diferentes computadores.
-👉 Reprodutibilidade: ao usar um ambiente virtual, você pode garantir que o seu código funcione da mesma maneira, independentemente do ambiente em que é executado.

## E como criar um ambiente virtual Python? 

Simples! Você pode usar o módulo `venv` integrado ao Python. Aqui está como criar um novo ambiente virtual: 🤓

- 1️⃣ Abra um terminal ou janela do prompt de comando.
- 2️⃣ Navegue até o diretório onde você deseja criar o ambiente virtual.
- 3️⃣ Digite `python3 -m venv myenv` (substitua `myenv` pelo nome que deseja dar ao seu ambiente virtual).
- 4️⃣ Pressione Enter para criar o ambiente virtual.

Depois de criar o ambiente virtual, você pode ativá-lo executando o seguinte comando: 🚀

No Windows: `myenv\Scripts\activate`

No Unix/MacOS: `source myenv/bin/activate`

Depois de ativar o ambiente virtual, você pode instalar quaisquer pacotes necessários usando o `pip`, sem afetar a instalação global do Python. Quando terminar de trabalhar no projeto, você pode desativar o ambiente virtual executando o comando `deactivate`. 💻