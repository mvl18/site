---
title: "Contornando Bloqueios indevidos ao Telegram pelo estado"
date: 2023-04-28T11:04:44-03:00
draft: false
categories: ["segurança"]
tags: ["vpn", "proxy", "telegram", "liberdade"]
authors: ["Julio Nunes Avelar"]
---

# Contornando Bloqueios indevidos ao Telegram pelo estado

No dia 26 de abril de 2023, o Telegram foi novamente bloqueado no Brasil, um ato inconstitucional que prejudica os direitos dos usuários. Neste post, vamos discutir como contornar esses bloqueios e ajudar a manter a privacidade e segurança dos usuários do Telegram.

## Como é feito o bloqueio do Telegram?

O bloqueio do Telegram é geralmente realizado por provedores de serviços de internet (ISPs) por ordem judicial ou ordem do governo. Isso significa que quando um usuário tenta acessar o Telegram, o ISP pode bloquear o acesso à rede do Telegram, impedindo que os usuários se conectem.

Os métodos utilizados para bloquear o acesso ao Telegram podem variar, mas geralmente envolvem o bloqueio de endereços IP específicos usados pelo aplicativo ou o bloqueio de certos domínios usados pelo Telegram. Essas técnicas podem ser aplicadas em nível de ISP ou em nível de rede mais amplo, como o bloqueio nacional.

## Contornando o bloqueio

Existem duas formas principais de contornar o bloqueio, usando VPN ou Proxy, cada uma com suas vantagens e desvantagens.

### Usando VPN

VPN é a sigla para "Virtual Private Network" ou "Rede Virtual Privada" em português. É uma tecnologia que permite criar uma conexão segura e privada entre um dispositivo (como um computador ou smartphone) e a internet, através de um servidor intermediário.

Em termos simples, uma VPN funciona como um túnel seguro que protege a comunicação do usuário, criptografando os dados enviados e recebidos pela internet. Isso ajuda a proteger a privacidade do usuário e a garantir que suas informações pessoais, como senhas e dados bancários, não sejam interceptadas por terceiros.

Prós:

- Qualquer conexão, mesmo em outros protocolos, é encapsulada, oferecendo um bom anonimato;
- Serve para qualquer aplicativo;
- Impede ataques de “Man-in-the-Middle”;
- Perfeita para conexões públicas, como Wi-Fi de restaurantes;
- Contorna bloqueios, como o que o Telegram está enfrentando no Brasil.

Contras:

- A internet inteira fica mais lenta;
- O servidor da VPN tem acesso a tudo o que o usuário faz, o que pode ser arriscado. Portanto, é importante usar VPNs seguras;
- IPs compartilhados com muitos clientes podem deixar a internet ainda mais lenta.

Algumas VPNs bem avaliadas (Recomendamos realizar uma pesquisa completa antes de escolher uma VPN por questões de segurança):

- ExpressVPN;
- NordVPN.
- CyberGhost VPN
- Private Internet Access (PIA)
- ProtonVPN
- VyprVPN
- Windscribe VPN

### Usando Proxy

Um proxy é um servidor que atua como intermediário entre um dispositivo (como um computador ou smartphone) e a internet. Quando um usuário se conecta a um proxy, o servidor encaminha as solicitações do usuário para os servidores da internet e, em seguida, devolve as respostas dos servidores de volta ao dispositivo do usuário. O Telegram tem a opção de usar um proxy de forma nativa.

Prós:

- Não afeta a velocidade da conexão em geral;
- Não interfere em outros aplicativos;
- Permite usar o Telegram.

Contras:

- Não oferece um bom anonimato;
- É possível identificar usuários que estão usando proxy para acessar o Telegram

Para utilizar um proxy no Telegram, é possível utilizar alguns links de configuração disponibilizados pelo próprio aplicativo que configuram a proxy automaticamente. Veja abaixo algumas opções de proxy:

[Digital Resistance proxy](tg://proxy?server=proxy.digitalresistance.dog&port=443&secret=ddd41d8cd98f00b204e9800998ecf8427e
)

[Cloudflare UK proxy](tg://proxy?server=cloudflare.com.nokia.com.co.uk.du_yo.want_to.with.this.www.googleso.com.baronia-intheparket.sbs&port=443&secret=ee000000000000000000000000000000006b65746161626f6e6c696e652e636f6d)


![Configurar proxy](/imgs/posts/telegram/proxy.png)

#### Tor Proxy

Uma possibilidade de proxy é a utilização da rede Tor como uma proxy. Para utilizar este recurso, você precisará ter o Tor instalado em seu computador ou dispositivo móvel, e a configuração é feita manualmente.

Configuração:

- Toda vez que for utilizar o proxy, você deve iniciar o Tor em seu dispositivo.
- Adicione a proxy ao seu Telegram e selecione para utilizá-la. Em dispositivos Android, acesse Configurações > Dados e Armazenamento > Configuração de Proxy. Na versão desktop, acesse Configurações > Avançadas > Tipo de Conexão.
- Habilite a opção "Usar Proxy".

![Habilitar proxy](/imgs/posts/telegram/ativarproxy.png)

- Configure uma nova proxy.
- Selecione o tipo SOCKS5.
- Coloque o servidor como "localhost" ou "127.0.0.1" e a porta como "9150".

![Configurar proxy](/imgs/posts/telegram/config.png)

- Salve a configuração e o Telegram tentará se conectar à proxy automaticamente.


Se você conhecer outras Proxys ou VPNs seguras que não foram listadas aqui, por favor, nos informe para que possamos atualizar esta lista e fornecer mais opções aos nossos leitores.

Lembre-se de que o uso de proxy não garante total anonimato e privacidade na internet. Além disso, tenha cuidado ao escolher a fonte de suas informações sobre proxies e VPNs, pois há muitos golpes e riscos envolvidos. Se tiver outras sugestões de proxies ou VPNs confiáveis, compartilhe conosco nos comentários para que possamos atualizar esta lista.