---
title: "Detecção Facial que reconhece se a pessoa esta usando mascara ou não"
date: 2023-04-28T09:42:46-03:00
draft: false
categories: ["machine learning"]
tags: ["python", "OpenCV"]
authors: ["Julio Nunes Avelar"]
---

# Detecção Facial que reconhece se a pessoa esta usando mascara ou não

![Exemplo](https://raw.githubusercontent.com/JN513/mask_detect/master/testes/teste.det.jpg)

E aí, dev? Hoje eu vim te apresentar um projeto bem interessante utilizando Python, com o intuito de detectar pessoas sem máscara facial, pois afinal, após o início da pandemia causada pelo coronavírus, vários locais determinaram como obrigatório o uso deste item. Mas sem mais enrolação, vamos lá! 


## 👋 Introdução

Vou mostrar como detectar, de forma automática, quem está ou não utilizando máscara em determinado ambiente. Como? Com visão computacional e aprendizagem de máquina (machine learning). 


## 🤨 Alto lá! Precisamos saber de dois termos antes de prosseguir nesta jornada.

👉 **Visão computacional** é o processo de modelagem e replicação da visão humana usando software e hardware. Na prática, temos como exemplo a necessidade de se identificar se alguém está ou não utilizando máscara facial, ou seja, a máquina estará replicando a visão e percepção humana sobre a situação.

👉 **Aprendizagem de máquina** é um subcampo da Inteligência Artificial, que trabalha com a ideia de que máquinas podem aprender sozinhas a partir de uma enorme quantidade de dados. No exemplo a ser apresentado aqui, a máquina irá aprender a detectar a existência de máscara ou não a partir de um banco de imagens com pessoas utilizando máscaras.



## 🤔 O que eu preciso saber para entender melhor o assunto?

👉 Linguagem de programação Python;
👉 Pip; 
👉 Numpy;
👉 Open CV;

Acompanhe até o final para ter dicas de materiais de estudo! 


## 🤯 O que usei no projeto?

Antes de mais nada, você pode ter acesso ao projeto pelos links abaixo:

👉 Versão no [Google Colab](https://colab.research.google.com/drive/145o1e8Z23aKkgBZT8cpcavjVAc8VHskp?authuser=1);
 👉 Versão em [código Python](https://github.com/JN513/mask_detect);
 
Agora vamos às dependências que você precisará instalar: 

👉 Versão 3.0 ou superior do Python; 
👉 Bibliotecas: 
* Tensor Flow; 
* Open CV; 
* Numpy;
* Cython;
* Codecov; 
* Pytest-cov; 
* Pytesseract; 
* Tesseract-ocr;
* Wand;
* Sklearn;
* Imutils;
* Matplotlib;
* PIL.

 ❗️ No [repositório](https://github.com/JN513/mask_detect) do projeto você consegue encontrá-las no arquivo requirements.txt a fim de baixá-las facilmente. Basta usar o seguinte comando no no terminal/cmd.

```bash 
pip3 install -r requirements.txt
``` 

## 🦾 Organização do projeto

O projeto conta com 3 arquivos: 

👉 **Arquivo de treino:** Para treinar o modelo, basta rodar o arquivo **treino.py**. ( É válido destacar que o projeto conta com banco de imagens com 690 imagens de pessoas com máscaras e 686, sem elas. Para adicionar uma imagem, basta colocá-la na pasta “dataset”).

👉 **Arquivo de detecção através de uma imagem:** Para realizar o processo de detecção, basta utilizar o arquivo detect_image.py e passar os argumentos **-i** e o **path da imagem**. 

Exemplo: 

```bash 
python3 detect_image.py -i testes/1.png
```

👉 **Arquivo de detecção através de um vídeo ou câmera:** Para utilizar a câmera padrão de seu dispositivo, basta rodar o arquivo **detect_camera.py**.  

Exemplo: 

 ```bash 
python3 detect_camera.py
``` 
Entretanto, caso queira usar uma câmera externa ou o aparelho eletrônico via droidcam, basta colocar o IP da câmera na linha 63.

Exemplo:

```bash
captura_video = cv2.VideoCapture("192.168.0.102:4224")
```

Por fim, para fazer uso de um vídeo, basta passar o **path** dele na mesma linha já citada.

## 📚 Hora do estudo, dev! 

Abaixo, cito algumas fontes de estudo para que você se aventure no mundo incrível da visão computacional e machine learning.

👉 Python:

  * <https://www.python.org/>

👉 Python Open CV:

  * <https://opencv-python-tutroals.readthedocs.io/>

👉 Numpy:

  * <https://numpy.org/>

👉 Tensor FLow: 

  * <https://www.tensorflow.org/>


## Resultados

### - Perda e Acurácia

![Perda e Acurácia](https://raw.githubusercontent.com/JN513/mask_detect/master/plot.png)

### - Acurácia

![Acurácia](https://raw.githubusercontent.com/JN513/mask_detect/master/plotloss.png)

### - Perda

![Perda](https://raw.githubusercontent.com/JN513/mask_detect/master/plotacc.png)



## Créditos:

Esse projeto foi baseado no projeto de [Gorpo](https://github.com/gorpo/Face-Recognition-Detector-de-Mascara-Python-Covid-19) (Como não sei o nome dele, fica o nick mesmo haha).

Revizão e correção do texto --> [Luisa Manoela Romão Sales](https://neps.academy/user/10702)