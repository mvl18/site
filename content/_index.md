---
title: "Home"
date: 2023-04-27
draft: false
---

{{< most-recent-box "posts" "Confira a última postagem de nosso blog!" >}}

# Bem-vindo ao Bzóide

O [Bzóide](https://bzoide.dev/posts/genesis/) é um blog de tecnologia voltado para o desenvolvimento de conteúdos e projetos relacionados à eletrônica, sistemas embarcados, IoT, segurança e outras áreas afins. Estamos abertos a qualquer tipo de conteúdo relacionado à tecnologia.

# Quem somos

Somos um grupo de alunos da [UNICAMP](https://www.unicamp.br/unicamp/), apaixonados por tecnologia e com vasta experiência em eletrônica, programação, sistemas embarcados e IoT. Nosso objetivo é compartilhar conhecimento, desenvolver projetos opensource e ajudar as pessoas a resolver problemas em seu dia a dia. Por isso, decidimos criar este blog para dividir nossas experiências e ajudar outras pessoas a aprender novas habilidades.

# Objetivos

Nossos objetivos principais são:

- Disseminar conhecimentos sobre Eletrônica, Sistemas embarcados, IoT, segurança, entre outros temas relacionados à tecnologia;
- Desenvolver e divulgar projetos opensources em todas as áreas citadas acima;
- Ajudar as pessoas a aprender novos conteúdos e a resolver problemas cotidianos relacionados à tecnologia.

# Participação de alunos da Unicamp

Nosso blog é mantido e desenvolvido por alunos da [UNICAMP](https://www.unicamp.br/unicamp/), o que nos permite ter uma forte ligação com a universidade. Esse vínculo nos dá acesso a uma ampla gama de recursos e conhecimentos, o que nos permite desenvolver conteúdos e projetos de alta qualidade.

# Entidades relacionadas ao Blog

O criador deste blog é membro de duas entidades da [UNICAMP](https://www.unicamp.br/unicamp/): [LKCAMP](https://lkcamp.dev) e [EMBARCAÇÕES](https://gitlab.com/embarcoes). Por esse motivo, é possível que haja colaboração e conteúdos em comum entre essas entidades e o Bzoide. Isso só enriquece ainda mais o nosso conhecimento e a nossa rede de contatos.